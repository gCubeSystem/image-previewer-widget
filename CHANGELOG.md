
# Changelog for Portal Auth Library

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v1.3.0-SNAPSHOT] - 2023-03-31

- ported to git

## [v1.2.0] - 2017-05-22

- Updated style of arrows and loader
- Added possibility to override click next and click prev

## [v1.1.0] - 2016-04-02

- Image orientation handled by means of the EXIF metadata

## [v1.0.0] - 2016-01-22

- First release
