package org.gcube.portlets.widgets.imagepreviewerwidget.client;

public interface CarouselInterface {
	 void onClickPrev();
	 void onClickNext();
}
